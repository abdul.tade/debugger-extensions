// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the QUANTEXT_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// QUANTEXT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef QUANTEXT_EXPORTS
#define QUANTEXT_API __declspec(dllexport)
#else
#define QUANTEXT_API __declspec(dllimport)
#endif

#include <DbgEng.h>

// This class is exported from the dll

#define GETBIT(v, b) ((static_cast<ULONG64>(v) >> b) & 0x1ull)

#pragma pack(push, 1)
typedef struct Addr32 {
	UINT8 P : 1;
	UINT8 RW : 1;
	UINT8 US : 1;
	UINT8 PWT : 1;
	UINT8 PCD : 1;
	UINT8 A : 1;
	UINT8 D : 1;
	UINT8 PS : 1;
	typedef union {
		typedef struct {
			UINT8 G : 1;
			UINT8 AVL : 3;
			UINT8 PAT : 1;
			UINT8 PhyAddr32to39 : 8;
			UINT8 RSVD : 1;
			UINT16 PhyAddr22to31 : 10;
		} Rest1;

		typedef struct {
			UINT8 AVL : 4;
			UINT32 PhyAddr12to32 : 20;
		} Rest0;
	} Optional; // changes depending on the value of PS bit
} *PAddr32;
#pragma pack(pop)

extern "C" HRESULT QUANTEXT_API CALLBACK DebugExtensionInitialize(PULONG Version, PULONG Flags);

extern "C" void QUANTEXT_API CALLBACK DebugExtensionUninitialize();

extern "C" void QUANTEXT_API CALLBACK DebugExtensionNotify(ULONG Notify, ULONG64 Argument);

extern "C" HRESULT QUANTEXT_API CALLBACK fmtaddr(PDEBUG_CLIENT4 Client, PCSTR Args);
