// QuantExt.cpp : Defines the exported functions for the DLL.

#include "pch.h"
#include "framework.h"
#include "QuantExt.h"
#include "ArgumentParser.h"
#include "Lexer.h"
#include "Parser.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <filesystem>
#include <fstream>


PDEBUG_CONTROL debugControl{ nullptr };

enum class Type : uint64_t {
	Int, Long, String, Unsigned_Int, Unsigned_Long,
	Unsigned_Long_Long, Float, Double
};

struct ValueDescriptor {
	std::string type;
	void* ptr;
};

struct FileData {

	FileData() = delete;

	/*Takes ownership of buffer, beware to prevent heap corruption due to double free*/
	FileData(char* ptr, size_t size)
		: ptr(ptr),
		  size(size)
	{}

	const std::string& show() & {
		if (str == "")
			str = {ptr , size};
		return str;
	}

	bool isNull() const {
		return ptr == nullptr;
	}

	~FileData() noexcept {
		if (ptr)
			delete[] ptr;
	}
private:
	char* ptr{ 0 };
	size_t size{ 0 };
	std::string str = "";
};

extern "C" HRESULT QUANTEXT_API CALLBACK DebugExtensionInitialize(PULONG Version, PULONG Flags)
{
	*Version = DEBUG_EXTENSION_VERSION(1, 0);
	*Flags = 0;
	return S_OK;
}

extern "C" void QUANTEXT_API CALLBACK DebugExtensionUninitialize()
{
}

extern "C" void QUANTEXT_API CALLBACK DebugExtensionNotify(ULONG Notify, ULONG64 Argument)
{
	UNREFERENCED_PARAMETER(Argument);
	switch (Notify)
	{
	case DEBUG_NOTIFY_SESSION_ACTIVE:
		break;
	case DEBUG_NOTIFY_SESSION_INACTIVE:
		break;
	case DEBUG_NOTIFY_SESSION_ACCESSIBLE:
		break;
	case DEBUG_NOTIFY_SESSION_INACCESSIBLE:
		break;
	default:
		break;
	}
}

static void __stdcall format32bitAddr(PDEBUG_CONTROL debugControl, ULONG64 addr) {
	auto v = reinterpret_cast<PAddr32>(&addr);
	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS,
		"32 bit Virtual Address\n\tP : %d\n\tRW : %d\n"
		"\tUS : %d\n\tPWT : %d\n\tPCD : %d\n\tA : %d\n"
		"\tD : %d\n\t PS: 1\n"
		, v->P, v->RW, v->US, v->PWT, v->PCD, v->A);
}

static void __stdcall format64bitAddr(PDEBUG_CONTROL debugControl, ULONG64 addr) {
	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "Formatting Address %p\n", addr);
	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS,
		"\n***** 64 bit Virtual Address Mapping *****\n\t"
		"Page Offset: 0x%08x\n\t"
		"Page Table Entry: 0x%08x\n\t"
		"Page Table: 0x%08x\n\t"
		"Page Directory: 0x%08x\n\t"
		"Page Directory Table: 0x%08x\n\t"
		"Unused: 0x%08x\n",
		addr & ((1 << 12) - 1),
		(addr >> 12) & ((1 << 9) - 1),
		(addr >> 21) & ((1 << 9) - 1),
		(addr >> 30) & ((1 << 9) - 1),
		(addr >> 39) & ((1 << 9) - 1),
		(addr >> 48) & ((1 << 9) - 1)
	);
}

static bool __stdcall isHex(PCSTR arg) {
	return std::string{ arg }.find("0x") == 0;
}

static HRESULT __stdcall setDebugControlHandle(PDEBUG_CLIENT4 Client) {
	auto hr = Client->QueryInterface(__uuidof(IDebugControl),
		(void**)&debugControl);
	return hr;
}

static std::vector<std::string> split(PCSTR str, char delimeter) {

	std::stringstream ss{ str };
	std::vector <std::string> v{};
	std::string token;

	while (std::getline(ss, token, delimeter)) {
		v.emplace_back(token);
	}

	return v;
}

static PVOID loadFromAddr(ULONG64 addr, size_t size) {
	auto ptr = reinterpret_cast<void*>(addr);
	auto dataPtr = new char[size];
	RtlCopyMemory(dataPtr, ptr, size);
	return dataPtr;
}


static FileData readFile(PDEBUG_CONTROL debugControl, const std::string& filename) {
	auto&& filepath = std::filesystem::path{ filename };

	if (!std::filesystem::exists(filepath)) {
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "The file %s does not exist\n", filename.c_str());
		return { nullptr, 0 };
	}

	auto filesize = std::filesystem::file_size(filepath);
	auto dataptr = new char[filesize];

	std::ifstream inputstream;
	inputstream.open(filename, std::ios::binary);
	inputstream.read(dataptr, filesize);
	inputstream.close();

	return { dataptr, filesize };
}


size_t computeTypeSize(ASTNodePtr typenode) {
	const std::string &word = typenode->value;
	size_t ptrsize = sizeof(void*);
	if (word == "string") {
		const auto& child = typenode->children[0];
		auto charlength = std::strtoull(child->value.c_str(), nullptr, 10);
		if (!charlength)
			throw std::invalid_argument{ "Cannot have a string char length of zero in struct" };
		return charlength;
	}
	
	if (word == "char") return sizeof(char);
	if (word == "charptr") return ptrsize;
	if (word == "uchar") return sizeof(unsigned char);
	if (word == "ucharptr") return ptrsize;
	if (word == "int") return sizeof(int);
	if (word == "intptr") return ptrsize;
	if (word == "uint") return sizeof(unsigned int);
	if (word == "uintptr") return ptrsize;
	if (word == "long") return sizeof(long);
	if (word == "longptr") return ptrsize;
	if (word == "longlong") return sizeof(long long);
	if (word == "longlongptr") return ptrsize;
	if (word == "ulong") return sizeof(unsigned long);
	if (word == "ulongptr") return ptrsize;
	if (word == "ulonglong") return sizeof(unsigned long long);
	if (word == "ulonglongptr") return ptrsize;
	if (word == "float") return sizeof(float);
	if (word == "floatptr") return ptrsize;
	if (word == "double") return sizeof(double);
	if (word == "doubleptr") return ptrsize;
}

static size_t computeStructSize(ASTNodePtr ast) {
	size_t size = 0;
	for (auto& child : ast->children) {
		switch (child->type) {
		case NodeType::FIELD_DECL:
			size += computeTypeSize(child->children[1]);
		default:
			continue;
		}
	}
	return size;
}

static bool performCast(std::unique_ptr<char>& data, size_t size, ASTNodePtr ast) {
	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[+] Casting %s at address 0x%p to match specified Ast", ast->value, data.get());
	const auto& children = ast->children;
	auto dptr = data.get();
	auto count = 0ull;

	if (computeStructSize(ast) > size) {
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[-] Struct size bigger than data size\n");
		false;
	}

	while (count < size) {

	}
}

extern "C" HRESULT QUANTEXT_API CALLBACK cast(PDEBUG_CLIENT4 Client, PCSTR Args) {
	auto args = split(Args, ' ');
	auto hr = setDebugControlHandle(Client);

	if (hr != S_OK) {
		return hr;
	}

	auto&& required = std::unordered_set<std::string>{ "-a", "-f" };
	ArgumentParser argParser(debugControl, 4ul, required, std::unordered_set<std::string>{});

	if (args.size() != 4) {
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[+] require exact arguments\n");
		return S_FALSE;
	}

	const char* argArr[4] = { args[0].c_str(), args[1].c_str() , args[2].c_str(), args[3].c_str() };
	std::span<char*> ptrArgs{ const_cast<char**>(argArr), 4 };

	if (!argParser.parseArgs(ptrArgs)) {
		return S_FALSE;
	}

	auto addr = argParser.getValue<std::string>("-a");
	auto filename = argParser.getValue<std::string>("-f");

	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS,
		"[+] QuantExt!cast Args { addr: %s, filename: %s }\n", addr.c_str(), filename.c_str());

	auto fileContent = readFile(debugControl, filename);

	if (!fileContent.isNull()) {
		return S_FALSE;
	}

	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[+] FileContent: %s\n", fileContent.show().c_str());

	Lexer lexer(fileContent.show());
	const auto& tokens = lexer.tokenize();

	for (auto& tk : lexer.tokenize()) {
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "Token(name: %s, type: %d)\n", tk.value.c_str(), tk.type);
	}

	Parser astParser(tokens);
	auto ast = astParser.parse();
	auto structSize = computeStructSize(ast);

	if (!structSize) {
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[-] An empty struct is not allowed\n");
		return S_FALSE;
	}

	auto addrValue = isHex(addr.c_str()) ?
		std::strtoull(addr.c_str(), nullptr, 16) : std::strtoull(addr.c_str(), nullptr, 10);

	std::unique_ptr<char> data(reinterpret_cast<char*>(loadFromAddr(addrValue, structSize)));

	if (!performCast(data, structSize, ast))
		return S_FALSE;

	return S_OK;
}

extern "C" HRESULT QUANTEXT_API CALLBACK fmtaddr(PDEBUG_CLIENT4 Client, PCSTR Args)
{
	ULONG64 addr{ 0 };
	auto hr = setDebugControlHandle(Client);

	if (hr != S_OK) {
		return hr;
	}

	addr = isHex(Args) ?
		std::strtoull(Args, nullptr, 16) : std::strtoull(Args, nullptr, 10);

	debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[+] fmtaddr running...\n");

	debugControl->IsPointer64Bit() == S_OK ?
		format64bitAddr(debugControl, addr) : format32bitAddr(debugControl, addr);

	return S_OK;
}




