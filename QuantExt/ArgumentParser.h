#pragma once

#include <DbgEng.h>
#include <iostream>
#include <span>
#include <cstring>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

struct Arguments {

	static Arguments make_arguments(PDEBUG_CONTROL debugControl, const std::unordered_set<std::string>& required, const std::unordered_set<std::string>& optionals) {
		return Arguments{ debugControl, required, optionals };
	}

	Arguments(PDEBUG_CONTROL debugControl, const std::unordered_set<std::string>& required, const std::unordered_set<std::string>& optionals)
		: required(required),
		optionals(optionals),
		debugControl(debugControl)
	{}

	bool isRequired(const char* argName)
	{
		return required.find(std::string{ argName }) != required.end();
	}

	bool isOptional(const char* argName) {
		return optionals.find(argName) != optionals.end();
	}

	bool argumentExists(const char* argName) {
		return isRequired(argName) || isOptional(argName);
	}

	std::unordered_set<std::string> getArguments() {
		auto r = required;
		for (auto& p : optionals) {
			if (r.count(p)) {
				debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "Argument %s must be optional or required only\n", p.c_str());
				return {};
			}
			r.insert(p);
		}
		return r;
	}

private:
	std::unordered_set<std::string> required;
	std::unordered_set<std::string> optionals;
	PDEBUG_CONTROL debugControl;
};

struct ArgumentParser
{
	using str = const char*;

	ArgumentParser(PDEBUG_CONTROL debugControl, size_t maxArgs, const std::unordered_set<std::string>& required, const std::unordered_set<std::string>& optionals)
		: maxArgs(maxArgs),
		arguments(Arguments::make_arguments(debugControl, required, optionals)),
		debugControl(debugControl)
	{}

	bool parseArgs(std::span<char*>& ap)
	{
		if (ap.size() < maxArgs) {
			debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[-] Insufficient number of arguments\n");
			return false;
		}

		auto argSize = ap.size();
		if ((argSize == 0) || argSize % 2 != 0) {
			debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[-] Invalid argument format\n\tCorrect format: [PROG_NAME] [optname1] [optValue1] [optname2] [optValue2] ...\n");
			false;
		}

		for (auto option : arguments.getArguments()) {

			auto it = std::find_if(ap.begin(), ap.end(), [&](char* value) {
				return option == value;
				});

			if (arguments.isRequired(option.c_str()) && it == ap.end()) {
				debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[-] Cannot find required option: %s\n", option.c_str());
				return false;
			}

			if (arguments.isOptional(option.c_str()) && it == ap.end()) {
				continue;
			}

			auto indx = std::distance(ap.begin(), it);
			optVal[ap[indx]] = ap[indx + 1];
		}

		return true;
	}

	bool argumentExist(str optName) {
		return arguments.argumentExists(optName);
	}

	template <typename T>
	T getValue(str optName)
	{
		static_assert(false, "Unsupported value type");
	}

	template <>
	int getValue<int>(str optName)
	{
		auto&& val = getArg(optName);
		return std::atoi(val.c_str());
	}

	template <>
	unsigned long getValue<unsigned long>(str optName)
	{
		auto&& val = getArg(optName);
		return std::strtoul(val.c_str(), nullptr, 10);
	}

	template <>
	unsigned long long getValue<unsigned long long>(str optName)
	{
		auto&& val = getArg(optName);
		return std::strtoul(val.c_str(), nullptr, 10);
	}

	template <>
	double getValue<double>(str optName)
	{
		auto&& val = getArg(optName);
		return std::strtod(val.c_str(), nullptr);
	}

	template <>
	float getValue<float>(str optName)
	{
		auto&& val = getArg(optName);
		return std::strtof(val.c_str(), nullptr);
	}

	template <>
	char getValue<char>(str optName)
	{
		auto&& val = getArg(optName);
		return val[0];
	}

	template <>
	std::string getValue<std::string>(str optName) {
		return getArg(optName);
	}

private:
	std::string getArg(str optName) {
		if (search(optName)) {
			return optVal.at(optName);
		}
		else if (arguments.isOptional(optName)) {
			debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "Cannot find optional argument: %s\n", optName);
			return "";
		}
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, "[-] Cannot find option: %s\n", optName);
		return "";
	}

	bool search(str optName)
	{
		return arguments.argumentExists(optName);
	}

private:
	std::unordered_map<std::string, std::string> optVal;
	Arguments arguments;
	size_t maxArgs;
	PDEBUG_CONTROL debugControl;
};

