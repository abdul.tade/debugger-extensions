#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <memory>

enum class NodeType {
    STRUCT_DECL,
    FIELD_DECL,
    TYPE,
    IDENTIFIER,
    NUMBER,
    UNKNOWN
};

struct ASTNode {
    NodeType type;
    std::string value;
    std::vector<std::shared_ptr<ASTNode>> children;

    ASTNode(NodeType type, const std::string& value) : type(type), value(value) {}
};

using ASTNodePtr = std::shared_ptr<ASTNode>;
