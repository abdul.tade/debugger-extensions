#pragma once

#include <string>

enum class _TokenType {
    STRUCT,
    IDENTIFIER,
    CHAR,
    UCHAR,
    INT,
    UINT,
    LONG,
    ULONG,
    LONGLONG,
    ULONGLONG,
    FLOAT,
    DOUBLE,
    STRING,
    VOIDPTR,
    CHARPTR,
    UCHARPTR,
    INTPTR,
    UINTPTR,
    LONGPTR,
    ULONGPTR,
    LONGLONGPTR,
    ULONGLONGPTR,
    FLOATPTR,
    DOUBLEPTR,
    END,
    LPAREN,
    RPAREN,
    NUMBER,
    UNKNOWN
};

struct Token {
    _TokenType type;
    std::string value;
};