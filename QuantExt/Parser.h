#pragma once


#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <sstream>
#include "Token.h"


enum class NodeType {
    STRUCT_DECL,
    FIELD_DECL,
    TYPE,
    IDENTIFIER,
    NUMBER,
    UNKNOWN
};

struct ASTNode {
    NodeType type;
    std::string value;
    std::vector<std::shared_ptr<ASTNode>> children;

    ASTNode(NodeType type, const std::string& value) : type(type), value(value) {}
};

using ASTNodePtr = std::shared_ptr<ASTNode>;

class Parser {
public:

    static std::string printAST(const ASTNodePtr& node, int indent = 0) {
        std::stringstream ss{};

        for (int i = 0; i < indent; ++i) {
            ss << "  ";
            ss << "Node(Type: ";
            switch (node->type) {
            case NodeType::STRUCT_DECL: ss << "STRUCT_DECL"; break;
            case NodeType::FIELD_DECL: ss << "FIELD_DECL"; break;
            case NodeType::TYPE: ss << "TYPE"; break;
            case NodeType::IDENTIFIER: ss << "IDENTIFIER"; break;
            case NodeType::NUMBER: ss << "NUMBER"; break;
            default: ss << "UNKNOWN"; break;
            }
            ss << ", Value: " << node->value << ")" << std::endl;

            for (const auto& child : node->children) {
                printAST(child, indent + 1);
            }
        }
        return ss.str();
    }

    Parser(const std::vector<Token>& tokens) : tokens(tokens), position(0) {}

    Parser() = delete;

    ASTNodePtr parse() {
        return parseStruct();
    }

private:
    std::vector<Token> tokens;
    size_t position;

    ASTNodePtr parseStruct() {
        ASTNodePtr structNode = std::make_shared<ASTNode>(NodeType::STRUCT_DECL, "");
        if (match(_TokenType::STRUCT)) {
            structNode->value = consume(_TokenType::IDENTIFIER).value;
            while (!match(_TokenType::END)) {
                structNode->children.push_back(parseField());
            }
            consume(_TokenType::END);
        }
        return structNode;
    }

    ASTNodePtr parseField() {
        ASTNodePtr fieldNode = std::make_shared<ASTNode>(NodeType::FIELD_DECL, "");
        fieldNode->children.push_back(std::make_shared<ASTNode>(NodeType::IDENTIFIER, consume(_TokenType::IDENTIFIER).value));
        fieldNode->children.push_back(parseType());
        return fieldNode;
    }

    ASTNodePtr parseType() {
        Token typeToken = consume();
        ASTNodePtr typeNode = std::make_shared<ASTNode>(NodeType::TYPE, typeToken.value);
        if (typeToken.type == _TokenType::STRING && match(_TokenType::LPAREN)) {
            consume(_TokenType::LPAREN);
            typeNode->children.push_back(std::make_shared<ASTNode>(NodeType::NUMBER, consume(_TokenType::NUMBER).value));
            consume(_TokenType::RPAREN);
        }
        return typeNode;
    }

    Token consume() {
        if (position < tokens.size()) {
            return tokens[position++];
        }
        return { _TokenType::UNKNOWN, "" };
    }

    Token consume(_TokenType expectedType) {
        Token token = consume();
        if (token.type != expectedType) {
            std::cerr << "Unexpected token: " << token.value << std::endl;
        }
        return token;
    }

    bool match(_TokenType type) {
        if (position < tokens.size() && tokens[position].type == type) {
            return true;
        }
        return false;
    }
};
