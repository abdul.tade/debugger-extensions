#pragma once

enum class DataType : unsigned char {
	Int, Long, String, Float, Double,
	UnsignedInt, UnsignedLong, UnsignedLongLong,
	Custom
};