#pragma once

#include <DbgEng.h>
#include <cstdlib>

struct Utils {
	static void write(PDEBUG_CONTROL debugControl, PCSTR fmt, ...) {
		va_list argptr;
		va_start(argptr, fmt);
		debugControl->Output(DEBUG_OUTCTL_ALL_CLIENTS, fmt, argptr);
		va_end(argptr);
	}
};
