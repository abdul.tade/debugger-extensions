#pragma once

enum class _TokenType : unsigned char {
	STRUCT, END, IDENTIFIER, _EOF,
	TYPE
};