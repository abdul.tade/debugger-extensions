#include <DbgEng.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cctype>
#include "Token.h"

class Lexer {

    std::vector<Token> tokens;

public:
    Lexer(const std::string& input) : input(input), position(0) {}

    Lexer() = delete;

    std::vector<Token>& tokenize() & {
        while (position < input.length()) {
            char currentChar = input[position];
            if (std::isspace(currentChar)) {
                ++position;
            }
            else if (std::isalpha(currentChar)) {
                std::string word = readWhile(&Lexer::isAlphaNumeric);
                tokens.emplace_back(Token{ getKeywordType(word), word });
            }
            else if (std::isdigit(currentChar)) {
                std::string number = readWhile(&Lexer::isDigit);
                tokens.emplace_back(Token{ _TokenType::NUMBER, number });
            }
            else if (currentChar == '(') {
                tokens.emplace_back(Token{ _TokenType::LPAREN, "(" });
                ++position;
            }
            else if (currentChar == ')') {
                tokens.emplace_back(Token{ _TokenType::RPAREN, ")" });
                ++position;
            }
            else {
                tokens.emplace_back(Token { _TokenType::UNKNOWN, std::string(1, currentChar) });
                ++position;
            }
        }
        return tokens;
    }

private:
    std::string input;
    size_t position;

    std::string readWhile(bool (Lexer::* predicate)(char)) {
        std::string result;
        while (position < input.length() && (this->*predicate)(input[position])) {
            result += input[position];
            ++position;
        }
        return result;
    }

    bool isAlphaNumeric(char c) {
        return std::isalnum(c) || c == '_';
    }

    bool isDigit(char c) {
        return std::isdigit(c);
    }

    _TokenType getKeywordType(const std::string& word) {
        if (word == "struct") return _TokenType::STRUCT;
        if (word == "char") return _TokenType::CHAR;
        if (word == "charptr") return _TokenType::CHARPTR;
        if (word == "uchar") return _TokenType::UCHAR;
        if (word == "ucharptr") return _TokenType::UCHARPTR;
        if (word == "int") return _TokenType::INT;
        if (word == "intptr") return _TokenType::INTPTR;
        if (word == "uint") return _TokenType::UINT;
        if (word == "uintptr") return _TokenType::UINTPTR;
        if (word == "long") return _TokenType::LONG;
        if (word == "longptr") return _TokenType::LONGPTR;
        if (word == "longlong") return _TokenType::LONGLONG;
        if (word == "longlongptr") return _TokenType::LONGLONGPTR;
        if (word == "ulong") return _TokenType::ULONG;
        if (word == "ulongptr") return _TokenType::ULONGPTR;
        if (word == "ulonglong") return _TokenType::ULONGLONG;
        if (word == "ulonglongptr") return _TokenType::ULONGLONGPTR;
        if (word == "float") return _TokenType::FLOAT;
        if (word == "floatptr") return _TokenType::FLOATPTR;
        if (word == "double") return _TokenType::DOUBLE;
        if (word == "doubleptr") return _TokenType::DOUBLEPTR;
        if (word == "string") return _TokenType::STRING;
        if (word == "end") return _TokenType::END;
        return _TokenType::IDENTIFIER;
    }
};